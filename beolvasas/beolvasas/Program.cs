﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace beolvasas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Írd be a neved");
            string str_name = Console.ReadLine();

            Console.WriteLine("Hello!");

            int a, b;
            Console.WriteLine("Adj meg egy egész számot");
            a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Adj meg egy másik egész számot");
            b = Convert.ToInt32(Console.ReadLine());

            if(a > 0) { Console.WriteLine("Az első szám pozitív"); }
            else if (a == 0) { Console.WriteLine("Az első szám nulla"); }
            else { Console.WriteLine("Az első szám negatív"); }

            if (b > 0) { Console.WriteLine("A második szám pozitív"); }
            else if (b == 0) { Console.WriteLine("A második szám nulla"); }
            else { Console.WriteLine("A második negatív"); }

            var c = a + b;

            Console.WriteLine($"Eredmény: {c}");
            if (c > 0) { Console.WriteLine("Az eredmény pozitív"); }
            else if (c == 0) { Console.WriteLine("Az eredmény nulla"); }
            else { Console.WriteLine("Az eredmény negatív"); }

            Console.ReadKey();
        }
    }
}
